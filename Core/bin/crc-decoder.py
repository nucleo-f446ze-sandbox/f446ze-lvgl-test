# Initial value used in CRC calculation -> This is constant!
initial_crc = 0xFF
# Polynomial Coefficient used in CRC calculation -> This is constant!
poly_coeff = 0xB7
# Test Input.
input_num = 0xC1
input_str = "Hello World\r\n"
# Value holder for CRC summation of all characters in input string.
sum_crc = 0

for ch in input_str:
    bindex = 0
    crc = ord(ch) ^ initial_crc
    while (bindex < 8):
        if ((crc >> 7) == 0):
            bindex = bindex + 1
            crc = (crc << 1) & 0xFF
        else :
            bindex = bindex + 1
            crc = (crc << 1) & 0xFF
            crc = (crc ^ poly_coeff) & 0xFF
    sum_crc += crc
    print("final crc value for each character in hexadecimal is " + hex(crc))

print("Summation of all character CRCs is " + hex(sum_crc))

